# Escribe un programa que lea repetidamente números hasta que el usuario introduzca “ﬁn”.

#__Autor:_ Pedro Vicente  Merchan Infante
#__Email__ pedro.merchan@unl.edu.ec

cont = 0
total = 0
lista = []

while True:

    valor = input("Digite un número entero (o 'fin' sin no desea ingresar mas numeros): ")
    lista.append(valor)
    if valor.lower() in "fin":
        break
    try:
        total += float (valor)
        cont  += 1
        mayor = max (lista)
        menor = min (lista)
    except ValueError:
        print("ERROR, no es un numero\nIntentalo de nuevo...")

print("El total es: ", total)
print("HaS digitado: ", cont, " numeros")
print("Lista de numero:", lista)
print("El numero mayor digitado  es: ", float(mayor))
print("El numero menor digitado es: ", float(menor))
